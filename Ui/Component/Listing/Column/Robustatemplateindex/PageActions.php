<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2021. All Rights Reserved.
 * See README.md for more info
 */

namespace Robusta\Template\Ui\Component\Listing\Column\RobustaTemplateindex;

/**
 * Class PageActions
 * extends \Magento\Ui\Component\Listing\Columns\Column
 */
class PageActions extends \Magento\Ui\Component\Listing\Columns\Column
{
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource["data"]["items"])) {
            foreach ($dataSource["data"]["items"] as & $item) {
                $name = $this->getData("name");
                $id = "X";
                if (isset($item["template_id"])) {
                    $id = $item["template_id"];
                }
                $item[$name]["edit"] = [
                    "href"=>$this->getContext()->getUrl(
                        "robusta_template/template/edit",
                        ["template_id"=>$id]
                    ),
                    "label"=>__("Edit")
                ];
                $item[$name]["delete"] = [
                    "href"=>$this->getContext()->getUrl(
                        "robusta_template/template/delete",
                        ["template_id"=>$id]
                    ),
                    "label"=>__("Delete"),
                    'confirm' => [
                        'title' => __('Delete Template'),
                        'message' => __('Are you sure?')
                    ]
                ];
            }
        }

        return $dataSource;
    }
}
