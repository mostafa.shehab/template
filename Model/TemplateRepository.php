<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2021. All Rights Reserved.
 * See README.md for more info
 */

namespace Robusta\Template\Model;

use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Api\SearchResultsInterfaceFactory;
use Magento\Framework\Exception\CouldNotDeleteException;
use Magento\Framework\Exception\CouldNotSaveException;
use Magento\Framework\Exception\NoSuchEntityException;

use Robusta\Template\Api\TemplateRepositoryInterface;
use Robusta\Template\Api\Data\TemplateInterface;
use Robusta\Template\Model\TemplateFactory;
use Robusta\Template\Model\ResourceModel\Template as ObjectResourceModel;
use Robusta\Template\Model\ResourceModel\Template\CollectionFactory;
use Robusta\Base\Traits\HasGetAppUrl;
use Magento\framework\Api\SortOrder;

/**
 * Class TemplateRepository
 * implements TemplateRepositoryInterface
 */
class TemplateRepository implements TemplateRepositoryInterface
{
    use HasGetAppUrl;

    protected $objectFactory;
    protected $objectResourceModel;
    protected $collectionFactory;
    protected $searchResultsFactory;

    /**
     * TemplateRepository constructor.
     *
     * @param TemplateFactory $objectFactory
     * @param ObjectResourceModel $objectResourceModel
     * @param CollectionFactory $collectionFactory
     * @param SearchResultsInterfaceFactory $searchResultsFactory
     */
    public function __construct(
        TemplateFactory $objectFactory,
        ObjectResourceModel $objectResourceModel,
        CollectionFactory $collectionFactory,
        SearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->objectFactory        = $objectFactory;
        $this->objectResourceModel  = $objectResourceModel;
        $this->collectionFactory    = $collectionFactory;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * @inheritDoc
     *
     * @throws CouldNotSaveException
     */
    public function save(TemplateInterface $object)
    {
        try {
            $this->objectResourceModel->save($object);
        } catch (\Exception $e) {
            throw new CouldNotSaveException(__($e->getMessage()));
        }
        return $object;
    }

    /**
     * @inheritDoc
     */
    public function getById($id)
    {
        $object = $this->objectFactory->create();
        $this->objectResourceModel->load($object, $id);
        if (!$object->getId()) {
            throw new NoSuchEntityException(__('Object with id "%1" does not exist.', $id));
        }
        return $object;
    }

    /**
     * @inheritDoc
     */
    public function delete(TemplateInterface $object)
    {
        try {
            $this->objectResourceModel->delete($object);
        } catch (\Exception $exception) {
            throw new CouldNotDeleteException(__($exception->getMessage()));
        }
        return true;
    }

    /**
     * @inheritDoc
     */
    public function deleteById($id)
    {
        return $this->delete($this->getById($id));
    }

    /**
     * @inheritDoc
     */
    public function getList(SearchCriteriaInterface $criteria)
    {
        $searchResults = $this->searchResultsFactory->create();
        $searchResults->setSearchCriteria($criteria);
        $collection = $this->collectionFactory->create();
        foreach ($criteria->getFilterGroups() as $filterGroup) {
            $fields = [];
            $conditions = [];
            foreach ($filterGroup->getFilters() as $filter) {
                $condition = $filter->getConditionType() ? $filter->getConditionType() : 'eq';
                $fields[] = $filter->getField();
                $conditions[] = [$condition => $filter->getValue()];
            }
            if ($fields) {
                $collection->addFieldToFilter($fields, $conditions);
            }
        }
        $searchResults->setTotalCount($collection->getSize());
        $sortOrders = $criteria->getSortOrders();
        if ($sortOrders) {
            /** @var SortOrder $sortOrder */
            foreach ($sortOrders as $sortOrder) {
                $collection->addOrder(
                    $sortOrder->getData()['field'],
                    ($sortOrder->getData()['direction'] == SortOrder::SORT_ASC) ? 'ASC' : 'DESC'
                );
            }
        }
        $collection->setCurPage($criteria->getCurrentPage());
        $collection->setPageSize($criteria->getPageSize());
        $objects = [];
        foreach ($collection as $objectModel) {
            $objects[] = $objectModel;
        }
        $searchResults->setItems($objects);
        return $searchResults;
    }

    public function loadLocaleNames($templateId)
    {
        return $this->objectResourceModel->loadLocaleNames($templateId);
    }

    public function deleteOldLocales($id)
    {
        return $this->objectResourceModel->deleteOldLocales($id);
    }

    public function syncLocales($id, $locales)
    {
        $this->deleteOldLocales($id);
        return $this->objectResourceModel->saveLocales($id, $locales);
    }

    public function buildResponseList(array $templates)
    {
        $templatesToBeReturned = [];
        foreach ($templates as $template) {
            $templateResponse = $this->buildResponse($template);
            if ($templateResponse != null) {
                $templatesToBeReturned[] = $templateResponse;
            }
        }
        return $templatesToBeReturned;
    }

    public function buildResponse(Template $template)
    {
        $response = [
            'template_id' => $template->getData('template_id'),
            'template_title' => $template->getTitle(),
            'image_url' => $template->getImage()
        ];

        return $response;
    }
}
