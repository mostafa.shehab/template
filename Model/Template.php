<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2021. All Rights Reserved.
 * See README.md for more info
 */

namespace Robusta\Template\Model;

/**
 * Class Template
 * extends \Magento\Framework\Model\AbstractModel
 */
class Template extends \Magento\Framework\Model\AbstractModel implements
    \Robusta\Template\Api\Data\TemplateInterface,
    \Magento\Framework\DataObject\IdentityInterface
{
    const CACHE_TAG = 'robusta_template_template';

    /**
     * Init
     */
    protected function _construct() // phpcs:ignore PSR2.Methods.MethodDeclaration
    {
        $this->_init(\Robusta\Template\Model\ResourceModel\Template::class);
    }

    /**
     * @inheritDoc
     */
    public function getIdentities()
    {
        return [self::CACHE_TAG . '_' . $this->getId()];
    }

    public function getTitle()
    {
        $title = $this->getData('title');
        if ($title === null) {
            $title = $this->getData('movie_title');
        }
        return $title;
    }

    public function getImage()
    {
        return $this->getData('image_url');
    }

    //TODO Add additional getters for each attribute
    // If the attribute has a default and localized value follow the getTitle format
    // else follow the getImage format
}
