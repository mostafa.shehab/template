<?php declare(strict_types=1);

namespace Robusta\Template\Model\Resolver;

use Magento\Framework\Api\FilterBuilder;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\GraphQl\Config\Element\Field;
use Magento\Framework\GraphQl\Query\ResolverInterface;
use Magento\Framework\GraphQl\Schema\Type\ResolveInfo;
use Robusta\Template\Api\TemplateRepositoryInterface;
use Psr\Log\LoggerInterface;
use Magento\Framework\Api\SortOrderBuilder;

class TemplatesResolver implements ResolverInterface
{

    private $searchCriteriaBuilder;

    private $templatesRepository;

    private $filterBuilder;

    private $logger;

    private $sortOrderBuilder;

    private $args;

    public function __construct(
        SearchCriteriaBuilder $searchCriteriaBuilder,
        TemplateRepositoryInterface $templatesRepositoryInterface,
        FilterBuilder $filterBuilder,
        LoggerInterface $logger,
        SortOrderBuilder $sortOrderBuilder
    ) {
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->templatesRepository = $templatesRepositoryInterface;
        $this->filterBuilder = $filterBuilder;
        $this->logger = $logger;
        $this->sortOrderBuilder = $sortOrderBuilder;
        $this->args = [];
    }

    private function filter()
    {
        $filter = $this->filterBuilder
            ->setField('is_active')
            ->setConditionType('eq')
            ->setValue('1')
            ->create();
        $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter]);
        
        //TODO Can add any additional filtering by calling the appropriate function and passing in the attribute name
        // Always keep the 'is_active' filter above
        $this   ->filterByNumericRange('template_id')
                ->filterByStringAttribute('template_title');   
    }

    private function filterByNumericRange($attribute)
    {
        if (isset($this->args['filter'][$attribute]) && $this->args['filter'][$attribute]) {
            if (isset($this->args['filter'][$attribute]['from']) && $this->args['filter'][$attribute]['from']) {
                $filter = $this->filterBuilder
                    ->setField($attribute)
                    ->setConditionType('from')
                    ->setValue($this->args['filter'][$attribute]['from'])
                    ->create();
                $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter]);
            }
            if (isset($this->args['filter'][$attribute]['to']) && $this->args['filter'][$attribute]['to']) {
                $filter = $this->filterBuilder
                    ->setField($attribute)
                    ->setConditionType('to')
                    ->setValue($this->args['filter'][$attribute]['to'])
                    ->create();
                $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter]);
            }
        }
        return $this;
    }

    private function filterByStringAttribute($attribute)
    {
        if (isset($this->args['filter'][$attribute]) && $this->args['filter'][$attribute]) {
            if (isset($this->args['filter'][$attribute]['like']) && $this->args['filter'][$attribute]['like']) {
                $filter = $this->filterBuilder
                    ->setField($attribute)
                    ->setConditionType('like')
                    ->setValue('%'.$this->args['filter'][$attribute]['like'].'%')
                    ->create();
                $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter]);
            } elseif (isset($this->args['filter'][$attribute]['eq']) && $this->args['filter'][$attribute]['eq']) {
                $filter = $this->filterBuilder
                    ->setField($attribute)
                    ->setConditionType('eq')
                    ->setValue($this->args['filter'][$attribute]['eq'])
                    ->create();
                $searchCriteria = $this->searchCriteriaBuilder->addFilters([$filter]);
            }
        }
        return $this;
    }

    private function sorting()
    {
        //TODO Can add any additional sorting by simply calling the sortByAttribute function passing in the attribute name
        $this->sortByAttribute('template_id');
        $this->sortByAttribute('template_title');
    }

    private function sortByAttribute($attribute)
    {
        if (isset($this->args['sort'][$attribute]) && $this->args['sort'][$attribute]) {
            $sortOrder = $this->sortOrderBuilder->setField($attribute)->setDirection($this->args['sort'][$attribute]);
            $searchCriteria = $this->searchCriteriaBuilder->setSortOrders([$sortOrder]);
        }
    }

    /**
     * @inheritDoc
     */
    public function resolve(Field $field, $context, ResolveInfo $info, array $value = null, array $args = null)
    { 
        $this->args = $args;
        
        $this->filter();
        $this->sorting();

        $searchCriteria = $this->searchCriteriaBuilder->create();
        $searchCriteria->setCurrentPage($args['currentPage']);
        $searchCriteria->setPageSize($args['pageSize']);
        $templateList = $this->templatesRepository->getList($searchCriteria);
        $templates = $templateList->getItems();
        
        return $this->templatesRepository->buildResponseList($templates);
    }
}
