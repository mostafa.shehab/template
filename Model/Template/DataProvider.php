<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2021. All Rights Reserved.
 * See README.md for more info
 */

namespace Robusta\Template\Model\Template;

use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Filesystem;
use Magento\Ui\DataProvider\AbstractDataProvider;
use Robusta\Template\Model\ResourceModel\Template\CollectionFactory;
use Magento\Catalog\Model\Category\FileInfo;
use Magento\Framework\App\ObjectManager;

/**
 * Class DataProvider
 * extends \Magento\Ui\DataProvider\AbstractDataProvider
 */
class DataProvider extends \Magento\Ui\DataProvider\AbstractDataProvider
{

    protected $collection;

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var array
     */
    protected $loadedData;

    /**
     * @var Filesystem
     */
    private $fileInfo;

    /**
     * @param string $name
     * @param string $primaryFieldName
     * @param string $requestFieldName
     * @param CollectionFactory $collectionFactory
     * @param DataPersistorInterface $dataPersistor
     * @param array $meta
     * @param array $data
     */
    public function __construct(
        $name,
        $primaryFieldName,
        $requestFieldName,
        CollectionFactory $collectionFactory,
        DataPersistorInterface $dataPersistor,
        \Magento\Framework\Filesystem\Io\File $fileSystemIo,
        array $meta = [],
        array $data = []
    ) {
        $this->collection = $collectionFactory->create();
        $this->dataPersistor = $dataPersistor;
        $this->fileSystemIo = $fileSystemIo;
        parent::__construct($name, $primaryFieldName, $requestFieldName, $meta, $data);
        $this->meta = $this->prepareMeta($this->meta);
    }

    /**
     * Prepares Meta
     *
     * @param array $meta
     * @return array
     */
    public function prepareMeta(array $meta)
    {
        return $meta;
    }

    /**
     * Get data
     *
     * @return array
     */
    public function getData()
    {
        if (isset($this->loadedData)) {
            return $this->loadedData;
        }
        $items = $this->collection->getItems();

        foreach ($items as $item) {
            $templateId = $item->getId();
            $this->loadedData[$templateId] = $item->getData();

            if (count($items) == 1) {
                $this->loadedData[$templateId]['locale_values'] = $this->collection->getResource()->loadLocaleNames($templateId);
            }
        }

        $data = $this->dataPersistor->get('robusta_template_template');
        if (!empty($data)) {
            $item = $this->collection->getNewEmptyItem();
            $item->setData($data);
            $this->loadedData[$item->getId()] = $item->getData();
            $this->dataPersistor->clear('robusta_template_template');
        }

        if ($this->loadedData) {
            foreach ($this->loadedData as $key => &$offer) {
                //TODO Replace 'image_url' with the attribute name in the locale table where you store the image url
                $this->handleImage($offer, $key, 'image_url');
            }
        }

        return $this->loadedData;
    }

    public function handleImage($template, $key, $fileId)
    {
        if (isset($template['locale_values'])) {
            foreach ($template['locale_values'] as $key_2 => $locale_value) {
                if (is_array($locale_value[$fileId])) {
                    foreach ($locale_value[$fileId] as $image) {
                        $fileName = $image['name'] ?? null;
                    }
                } else {
                    $fileName = $locale_value[$fileId];
                }
                $fileInfo = $this->getFileInfo();
                /** @var \Magento\Framework\Filesystem\Io\File $fileSystemIo **/
                $filePathInfo = $this->fileSystemIo->getPathInfo($fileName);
                $basename = $filePathInfo['basename'];
                if ($fileName && $fileInfo->isExist($fileName)) {
                    $stat = $fileInfo->getStat($fileName);
                    $mime = $fileInfo->getMimeType($fileName);
                    $this->loadedData[$key]['locale_values'][$key_2][$fileId] = [[
                        'name' => $basename,
                        'url' => $fileName,
                        'size' => isset($stat) ? $stat['size'] : 0,
                        'type' => $mime
                    ]];
                } else {
                    $this->loadedData[$key]['locale_values'][$key_2][$fileId] = [[
                        'name' => $basename,
                        'url' => $fileName,
                        'size' => 0,
                        'type' => ''
                    ]];
                }
            }
        }
    }

    private function getFileInfo()
    {
        if ($this->fileInfo === null) {
            $this->fileInfo = ObjectManager::getInstance()->get(FileInfo::class);
        }
        return $this->fileInfo;
    }
}
