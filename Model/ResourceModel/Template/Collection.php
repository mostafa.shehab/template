<?php

/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2021. All Rights Reserved.
 * See README.md for more info
 */

namespace Robusta\Template\Model\ResourceModel\Template;

use Robusta\Base\Traits\HasLocalization;

/**
 * Class Collection
 * extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
 */
class Collection extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    use HasLocalization;

    /**
     * @var string
     */
    private $localeTable;

    /**
     * Init
     */
    protected function _construct() // phpcs:ignore PSR2.Methods.MethodDeclaration
    {
        $this->_init(\Robusta\Template\Model\Template::class, \Robusta\Template\Model\ResourceModel\Template::class);
        //TODO Change the 'robusta_template_template_locale' value to match your module name
        // Example: Robusta/CategoryOffer ---> 'robusta_categoryoffer_offer_locale'
        $this->localeTable = $this->getTable('robusta_template_template_locale');
    }

    protected function _initSelect()
    {
        parent::_initSelect();

        $locale = $this->getLocale();
        $this->addBindParam(':template_locale', $locale);
        $this->getSelect()->joinLeft(
            ['locale_table' => $this->localeTable],
            'main_table.template_id = locale_table.template_id AND locale_table.locale = :template_locale',
            //TODO update with actual array fo attributes that you want retrieved from the locale table
            ['locale_table.title', 'locale_table.image_url']
        );
        $this->addFilterToMap('template_id', 'main_table.template_id');
        return $this;
    }
}
