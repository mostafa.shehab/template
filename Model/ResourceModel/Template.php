<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2021. All Rights Reserved.
 * See README.md for more info
 */

namespace Robusta\Template\Model\ResourceModel;

use Robusta\Base\Traits\HasLocalization;

/**
 * Class Template
 * extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
 */
class Template extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    use HasLocalization;

    /**
     * @var string
     */
    private $localeTable;

    /**
     * Init
     */
    protected function _construct() // phpcs:ignore PSR2.Methods.MethodDeclaration
    {
        //TODO Change the 'robusta_template_template' value to match your module name
        // Example: Robusta/CategoryOffer ---> 'robusta_categoryoffer_offer'
        // Same change in the localeTable variable
        //TODO Replace the 'template_id' with the actual primary key of your table
        $this->_init('robusta_template_template', 'template_id');
        $this->localeTable = 'robusta_template_template_locale';
    }

    protected function _getLoadSelect($field, $value, $object)
    {
        $select = parent::_getLoadSelect($field, $value, $object);
        $connection = $this->getConnection();
        $locale = $this->getLocale();

        $fieldId = $connection->quoteIdentifier($this->getMainTable() . '.' . $this->getIdFieldName());

        $templateCondition = $connection->quoteInto('locale_table.locale = ?', $locale);
        $select->joinLeft(
            ['locale_table' => $this->localeTable],
            //TODO Replace the 'template_id' with the actual primary key of your table
            "{$fieldId} = locale_table.template_id AND {$templateCondition}",
            //TODO update with actual array fo attributes that you want retrieved from the locale table
            ['title', 'image_url']
        );
        return $select;
    }

    public function loadLocaleNames($templateId)
    {
        $sql = $this->getConnection()->select()
            ->from(
                [$this->localeTable],
                //TODO update with actual array fo attributes that you want retrieved from the locale table
                ['locale', 'title', 'image_url']
            )->where("template_id = " . $templateId); //TODO Replace the 'template_id' with the actual primary key of your table
        return $this->getConnection()->fetchAll($sql);
    }

    public function deleteOldLocales($id)
    {
        $this->getConnection()->delete(
            $this->localeTable,
            //TODO Replace the 'template_id' with the actual primary key of your table
            ['template_id = ?' => $id]
        );
    }

    public function saveLocales($templateId, $templateLocaleData)
    {
        if (count($templateLocaleData) == 0) {
            return;
        }
        $inputs = [];
        foreach ($templateLocaleData as $templateLocale) {
            if (isset($templateLocale['image_url'][0]['url'])) {
                $templateLocale['image_url'] = $templateLocale['image_url'][0]['url'];
            }
            //TODO update with actual array fo attributes that you want saved in the locale table
            $inputs[] = [
                'template_id' => $templateId,
                'locale' => $templateLocale['locale'],
                'title' => $templateLocale['title'],
                'image_url' => $templateLocale['image_url'],
            ];
        }
        $this->getConnection()->insertMultiple($this->localeTable, $inputs);
    }
}
