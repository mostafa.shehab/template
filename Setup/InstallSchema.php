<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2021. All Rights Reserved.
 * See README.md for more info
 */

namespace Robusta\Template\Setup;

use Magento\Framework\DB\Adapter\AdapterInterface as Db;
use Magento\Framework\DB\Ddl\Table;

/**
 * Class InstallSchema
 * implements \Magento\Framework\Setup\InstallSchemaInterface
 */
class InstallSchema implements \Magento\Framework\Setup\InstallSchemaInterface
{
    /**
     * @inheritDoc
     * @throws \Zend_Db_Exception
     */
    public function install(
        \Magento\Framework\Setup\SchemaSetupInterface $setup,
        \Magento\Framework\Setup\ModuleContextInterface $context
    ) {
        $installer = $setup;
        $installer->startSetup();
        //START: install stuff
        //END:   install stuff

        //START table setup
		//TODO Modify table name to match your module
        if (!$installer->tableExists('robusta_template_template')) {
			$table = $installer->getConnection()->newTable(
				$installer->getTable('robusta_template_template')
			)
				//TODO Add/Remove any required attributes
				// However, keep the id, is_active, created_at and updated_at columns as they are a standard in any table
				->addColumn(
					'template_id',
					\Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
					null,
					[
						'identity' => true,
						'nullable' => false,
						'primary'  => true,
						'unsigned' => true,
					],
					'Template ID'
				)
				->addColumn(
					'template_title',
					\Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
					255,
					[
						'nullable' => false
					],
					'Template Name'
				)
				->addColumn(
					'is_active',
					\Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT,
					null,
					[
						'nullable' => false,
						'default' => '1'
					],
					'Is Active'
				)
				->addColumn(
					'created_at',
					\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
					null,
					[
						'nullable' => false,
						'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT
					],
					'Created At'
				)
				->addColumn(
					'updated_at',
					\Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP,
					null,
					[
						'nullable' => false,
						'default' => \Magento\Framework\DB\Ddl\Table::TIMESTAMP_INIT_UPDATE
					],
					'Updated At'
				)
				->setComment('Template Table');
			$installer->getConnection()->createTable($table);
		}

		//TODO Add/Remove any attribute you want to have a localized version of
		// The locale and template_id attributes are defaults (change 'template_id' to primary key of above table)
        $table = $installer->getConnection()->newTable(
            $installer->getTable('robusta_template_template_locale')
        )
			->addColumn(
				'locale',
				Table::TYPE_TEXT,
				255,
				[
            		'nullable' => true,
        		],
				 'Locale'
			)
			->addColumn(
				'template_id',
				Table::TYPE_INTEGER,
				null,
				[
            		'unsigned' => true,
        		],
				'Template ID'
			)
			->addColumn(
				'title',
				Table::TYPE_TEXT,
				255,
				[
            		'nullable' => true,
        		],
				'Template Title'
			)
			->addColumn(
				'image_url',
				Table::TYPE_TEXT,
				255,
				[
					'nullable' => true,
				],
				'Template Image'
			)
			->setComment('Template Locale Table');

        $installer->getConnection()->createTable($table);
		
		//TODO modify the locale table name
		$tableName = 'robusta_template_template_locale';

        $setup->getConnection()->addForeignKey(
			$setup->getFkName($tableName, 'template_id', 'robusta_template_template', 'template_id'),
			$setup->getTable($tableName), 'template_id', $setup->getTable('robusta_template_template'), 'template_id', Db::FK_ACTION_CASCADE, false
		);
        //END   table setup
        $installer->endSetup();
    }
}
