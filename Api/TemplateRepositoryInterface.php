<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2021. All Rights Reserved.
 * See README.md for more info
 */

namespace Robusta\Template\Api;

use Robusta\Template\Api\Data\TemplateInterface;
use Magento\Framework\Api\SearchCriteriaInterface;

/**
 * Interface TemplateRepositoryInterface
 *
 * @api
 */
interface TemplateRepositoryInterface
{
    /**
     * Create or update a Template.
     *
     * @param TemplateInterface $page
     * @return TemplateInterface
     */
    public function save(TemplateInterface $page);

    /**
     * Get a Template by Id
     *
     * @param int $id
     * @return TemplateInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If Template with the specified ID does not exist.
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function getById($id);

    /**
     * Retrieve Templates which match a specified criteria.
     *
     * @param SearchCriteriaInterface $criteria
     */
    public function getList(SearchCriteriaInterface $criteria);

    /**
     * Delete a Template
     *
     * @param TemplateInterface $page
     * @return TemplateInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If Template with the specified ID does not exist.
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function delete(TemplateInterface $page);

    /**
     * Delete a Template by Id
     *
     * @param int $id
     * @return TemplateInterface
     * @throws \Magento\Framework\Exception\NoSuchEntityException If customer with the specified ID does not exist.
     * @throws \Magento\Framework\Exception\LocalizedException
     */
    public function deleteById($id);
}
