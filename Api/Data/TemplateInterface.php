<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2021. All Rights Reserved.
 * See README.md for more info
 */

namespace Robusta\Template\Api\Data;

/**
 * Interface TemplateInterface
 *
 * @api
 */
interface TemplateInterface
{
}
