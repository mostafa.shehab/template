<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2021. All Rights Reserved.
 * See README.md for more info
 */

namespace Robusta\Template\Controller\Adminhtml\Template;

use Magento\Backend\App\Action;
use Magento\Framework\App\Request\DataPersistorInterface;
use Magento\Framework\Exception\LocalizedException;
use Robusta\Template\Model\ImageUploader;

/**
 * Class Save
 * extends \Magento\Backend\App\Action
 */
class Save extends \Magento\Backend\App\Action
{
    //TODO Replace 'template_id' with the actual primary key of your table
    const ADMIN_RESOURCE_ID = 'template_id';
    const ADMIN_RESOURCE_EDIT = 'Robusta_Template::edit';
    const ADMIN_RESOURCE_CREATE = 'Robusta_Template::create';

    /**
     * @var DataPersistorInterface
     */
    protected $dataPersistor;

    /**
     * @var \Robusta\Template\Model\TemplateRepository
     */
    protected $objectRepository;

    /**
     * @var ImageUploader
     */
    private $imageUploader;

    /**
     * @var \Magento\Framework\Filesystem\Driver\File
     */
    private $file;

    /**
     * @var \Magento\Framework\Filesystem\DirectoryList
     */
    private $dir;

    /**
     * @param Action\Context $context
     * @param DataPersistorInterface $dataPersistor
     * @param ImageUploader $imageUploader
     * @param \Magento\Framework\Filesystem\Driver\File $file
     * @param \Magento\Framework\Filesystem\DirectoryList $dir
     * @param \Robusta\Template\Model\TemplateRepository $objectRepository
     */
    public function __construct(
        Action\Context $context,
        DataPersistorInterface $dataPersistor,
        \Robusta\Template\Model\TemplateRepository $objectRepository,
        \Magento\Framework\Filesystem\Driver\File $file,
        \Magento\Framework\Filesystem\DirectoryList $dir,
        ImageUploader $imageUploader
    ) {
        $this->dataPersistor = $dataPersistor;
        $this->objectRepository = $objectRepository;
        $this->dir = $dir;
        $this->file = $file;
        $this->imageUploader = $imageUploader;

        parent::__construct($context);
    }

    /**
     * Determines whether current user is allowed to access Action
     *
     * @return bool
     */
    protected function _isAllowed()
    {
        if ($this->getRequest()->getParam(self::ADMIN_RESOURCE_ID)) {
            return $this->_authorization->isAllowed(self::ADMIN_RESOURCE_EDIT);
        }
        return $this->_authorization->isAllowed(self::ADMIN_RESOURCE_CREATE);
    }

    /**
     * Save action
     *
     * @SuppressWarnings(PHPMD.CyclomaticComplexity)
     * @return \Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($data) {
            if (empty($data[self::ADMIN_RESOURCE_ID])) {
                $data[self::ADMIN_RESOURCE_ID] = null;
            }

            /** @var \Robusta\Template\Model\Template $model */
            $model = $this->_objectManager->create(\Robusta\Template\Model\Template::class);

            $id = $this->getRequest()->getParam(self::ADMIN_RESOURCE_ID);
            if ($id) {
                $model = $this->objectRepository->getById($id);
            }

            $model->setData($data);

            try {
                $this->objectRepository->save($model);
                $this->messageManager->addSuccess(__('You saved the template.'));
                $this->dataPersistor->clear('robusta_template_template');

                $templateRequestLocaleData = [];
                if (isset($data['locale_values'])) {
                    $templateRequestLocaleData = $data['locale_values'];
                    $this->validateLocaleValues($templateRequestLocaleData);
                }

                //TODO Replace 'image_url' with the attribute name in the locale table where you store the image url
                $imageFileKey = "image_url";
                $imagesLocalesFiles = $id ? array_map(function ($v) use ($imageFileKey) {
                    return $v[$imageFileKey];
                }, $this->objectRepository->loadLocaleNames($id)) : [];

                $templateRequestLocaleData = [];
                if (isset($data['locale_values'])) {
                    $data["locale_values"] = $this->handleFileCases(
                        $imagesLocalesFiles,
                        $data["locale_values"],
                        'image_url'
                    );
                    $templateRequestLocaleData = $data['locale_values'];
                }

                $this->objectRepository->syncLocales($model->getId(), $templateRequestLocaleData);

                if ($this->getRequest()->getParam('back')) {
                    return $resultRedirect->setPath('*/*/edit', [self::ADMIN_RESOURCE_ID => $model->getId(), '_current' => true]);
                }
                return $resultRedirect->setPath('*/*/');
            } catch (LocalizedException $e) {
                $this->messageManager->addError($e->getMessage());
            } catch (\Exception $e) {
                $this->messageManager->addException($e, __('Something went wrong while saving the data.'));
            }

            $this->dataPersistor->set('robusta_template_template', $data);
            return $resultRedirect->setPath('*/*/edit', [self::ADMIN_RESOURCE_ID => $this->getRequest()->getParam(self::ADMIN_RESOURCE_ID)]);
        }
        return $resultRedirect->setPath('*/*/');
    }

    private function moveFileFromTmp($name)
    {
        $this->imageUploader->moveFileFromTmp($name);
    }

    /**
     * @param array $oldFiles
     * @param array $data
     * @param string $key
     * @return array $data
     * @throws \Magento\Framework\Exception\FileSystemException
     */
    private function handleFileCases($oldFiles, $data, $key)
    {
        // Set Templates image urls for each locale
        foreach ($data as $localeKey => $localeData) {
            if (isset($localeData[$key . '_url'][0]['relative_url'])) {
                $this->moveFileFromTmp($localeData[$key . '_url'][0]['name']);
                $data[$localeKey][$key . '_url'] = $localeData[$key . '_url'][0]['relative_url'];
                $oldFileKey = array_search($data[$localeKey][$key . '_url'], $oldFiles);
                if ($oldFileKey !== false) {
                    unset($oldFiles[$oldFileKey]);
                }
            } elseif (isset($localeData[$key . '_url'][0]['url'])) {
                $data[$localeKey][$key . '_url'] = $localeData[$key . '_url'][0]['url'];
                $oldFileKey = array_search($data[$localeKey][$key . '_url'], $oldFiles);
                if ($oldFileKey !== false) {
                    unset($oldFiles[$oldFileKey]);
                }
            } else {
                $data[$localeKey][$key . '_url'] = null;
            }
        }

        foreach ($oldFiles as $file) {
            $this->deleteFile($file);
        }

        return $data;
    }

    /**
     * Delete File
     *
     * @param String $fileUrl
     */
    private function deleteFile($fileUrl)
    {
        if ($fileUrl) {
            $absolute_path = $this->dir->getPath('pub') . $fileUrl;
            if ($this->file->isExists($absolute_path)) {
                $this->file->deleteFile($absolute_path);
            }
        }
    }

    private function validateLocaleValues($locale_values)
    {
        $locales = [];
        foreach ($locale_values as $template_locale) {
            if (isset($template_locale['locale']) && !in_array($template_locale['locale'], $locales)) {
                $locales[] = $template_locale['locale'];
            } else {
                throw new LocalizedException(__("locale values duplicated"));
            }
        }
    }
}
