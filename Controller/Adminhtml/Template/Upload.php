<?php

/**
 *
 * @copyright 2021 Robusta Studio (https://www.robustastudio.com). All Rights Reserved.
 * @author Khaled Badenjki (khaled.badenjki@robustastudio.com)
 *
 */

namespace Robusta\Template\Controller\Adminhtml\Template;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Controller\ResultInterface;
use Robusta\Template\Model\ImageUploader;
use Magento\Framework\Controller\ResultFactory;

/**
 * Class Delete
 */
class Upload extends Action
{
    /**
     * Image uploader
     *
     * @var \Magento\Catalog\Model\ImageUploader
     */
    protected $imageUploader;

    /**
     * Upload constructor.
     *
     * @param Context $context
     * @param ImageUploader $imageUploader
     */
    public function __construct(
        Context $context,
        ImageUploader $imageUploader
    ) {
        parent::__construct($context);
        $this->imageUploader = $imageUploader;
    }

    /**
     * Upload file controller action
     *
     * @return ResultInterface
     */
    public function execute()
    {
        $data = $this->getRequest()->getPostValue();
        try {
            $result = $this->imageUploader->saveFileToTmpDir($data['param_name']);
        } catch (\Exception $e) {
            $result = ['error' => $e->getMessage(), 'errorcode' => $e->getCode()];
        }
        return $this->resultFactory->create(ResultFactory::TYPE_JSON)->setData($result);
    }
}
