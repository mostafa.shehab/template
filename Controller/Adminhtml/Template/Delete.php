<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2021. All Rights Reserved.
 * See README.md for more info
 */

namespace Robusta\Template\Controller\Adminhtml\Template;

/**
 * Class Delete
 * extends \Magento\Backend\App\Action
 */
class Delete extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Robusta_Template::template_edit';

    /**
     * @var \Robusta\Template\Model\TemplateRepository
     */
    protected $objectRepository;

    /**
     * Delete constructor.
     * @param \Robusta\Template\Model\TemplateRepository $objectRepository
     * @param \Magento\Backend\App\Action\Context $context
     */
    public function __construct(
        \Robusta\Template\Model\TemplateRepository $objectRepository,
        \Magento\Backend\App\Action\Context $context
    ) {
        $this->objectRepository = $objectRepository;

        parent::__construct($context);
    }

    public function execute()
    {
        // check if we know what should be deleted
        //TODO Change the 'template_id' with the actual primary key of your table
        $id = $this->getRequest()->getParam('template_id');
        /** @var \Magento\Backend\Model\View\Result\Redirect $resultRedirect */
        $resultRedirect = $this->resultRedirectFactory->create();
        if ($id) {
            try {
                // delete model
                $this->objectRepository->deleteById($id);
                // display success message
                $this->messageManager->addSuccess(__('You have deleted the Template.'));
                // go to grid
                return $resultRedirect->setPath('*/*/');
            } catch (\Exception $e) {
                // display error message
                $this->messageManager->addError($e->getMessage());
                // go back to edit form
                //TODO Change the 'template_id' with the actual primary key of your table
                return $resultRedirect->setPath('*/*/edit', ['template_id' => $id]);
            }
        }
        // display error message
        $this->messageManager->addError(__('We can not find a Template to delete.'));
        // go to grid
        return $resultRedirect->setPath('*/*/');
    }
}
