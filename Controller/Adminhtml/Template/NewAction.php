<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2021. All Rights Reserved.
 * See README.md for more info
 */

namespace Robusta\Template\Controller\Adminhtml\Template;

/**
 * Class NewAction
 * extends \Magento\Backend\App\Action
 */
class NewAction extends \Magento\Backend\App\Action
{
    const ADMIN_RESOURCE = 'Robusta_Template::template_edit';
    protected $resultPageFactory;
    public function __construct(
        \Magento\Backend\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $resultPageFactory
    ) {
        $this->resultPageFactory = $resultPageFactory;
        parent::__construct($context);
    }

    public function execute()
    {
        $page = $this->resultPageFactory->create();
        $page->getConfig()->getTitle()->set('New Template');
        return $page;
    }
}
