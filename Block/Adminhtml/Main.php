<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2021. All Rights Reserved.
 * See README.md for more info
 */

namespace Robusta\Template\Block\Adminhtml;

/**
 * Class Main
 * extends \Magento\Backend\Block\Template
 */
class Main extends \Magento\Backend\Block\Template
{
    public function _prepareLayout()
    {
        return $this;
    }
}
