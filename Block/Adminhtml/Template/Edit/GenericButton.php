<?php
/**
 * Copyright Robusta Studio (https://www.robustastudio.com) 2021. All Rights Reserved.
 * See README.md for more info
 */

namespace Robusta\Template\Block\Adminhtml\Template\Edit;

/**
 * Class GenericButton
 * GenericButton contains all the button methods
 */
class GenericButton
{
    //putting all the button methods in here.  No "right", but the whole
    //button/GenericButton thing seems -- not that great -- to begin with
    public function __construct(
        \Magento\Backend\Block\Widget\Context $context
    ) {
        $this->context = $context;
    }

    public function getBackUrl()
    {
        return $this->getUrl('*/*/');
    }

    public function getDeleteUrl()
    {
        //TODO Replace 'template_id' with the actual primary key of your table
        return $this->getUrl('*/*/delete', ['template_id' => $this->getObjectId()]);
    }

    public function getUrl($route = '', $params = [])
    {
        return $this->context->getUrlBuilder()->getUrl($route, $params);
    }

    public function getObjectId()
    {
        //TODO Replace 'template_id' with the actual primary key of your table
        return $this->context->getRequest()->getParam('template_id');
    }
}
